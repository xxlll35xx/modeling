#!/bin/bash

# Create test file for bayesrisk_create_cli.py

cat ../modeling/data/data.psv | bayesrisk_create_cli -x 1-2 -y 4 -a 25 -b 75 > ../modeling/data/bayestest.result.json
