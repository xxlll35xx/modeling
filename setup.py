from setuptools import setup, find_packages

setup(
	# package name
	name='modeling',
	# package version
	version='0.1',
	# package description
	description='Utilities to create predictive models',
	# link to package github or website
	url='https://xxlll35xx@bitbucket.org/xxlll35xx/modeling.git',
	# package author
	author='Leland Lockhart',
	# author contact email
	author_email='leland.lockhart@utexas.edu',
	# license
	license='MIT',
	# package to install, i.e., the name of this package
	packages=find_packages(),
	# list of dependencies if required and if on PyPi
	# optional
	install_requires=[],
	# A boolean (True or False) flag specifying whether the project can be safely installed and run from a zip file.
	zip_safe=False,
	# ensure Nose is installed for testing during installation
	# optional
	test_suite = 'nose.collector',
	tests_require = ['nose'],
	# path to any command line scripts
	# this is the easy way to include scripts, but these scripts aren't easily testable
	# optional
	#scripts = ['bin/bayesrisk_create.py'],
	# alternatively, scripts included via entry_points can be included in the test suite
	# optional
	entry_points = {
		'console_scripts':['bayesrisk_create_cli=modeling.bayesrisk_create_cli:main',
						'bayesrisk_apply_cli=modeling.bayesrisk_apply_cli:main',
						'hashjoin=modeling.hashjoin:main',
						'ks=modeling.ks:main',
						'metrics=modeling.metrics:main']
		},
	# include additional data in package install
	# optional
	include_package_data=True
	)

