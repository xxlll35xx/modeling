#!/usr/bin/env python
from modeling.bayesrisk_create_cli import make_risk
from modeling.util import parseFields
import argparse
import json
import sys

def main():
	# set argparse options
	USAGE = 'cat FILE | bayesrisk_create.py [arguments] > OUTPUT'
	DESCRIPTION = 'Create Bayesian risk tables given a categorical variable and a dichomotous outcome.'
	parser = argparse.ArgumentParser(description = DESCRIPTION, usage = USAGE, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("-a", "--a", dest = "A", help = "a parameter for Bayesian smoothing", required = True,
						action = "store", type = float)
	parser.add_argument("-b", "--b", dest = "B", help = "b parameter for Bayesian smoothing", required = True,
						action = "store", type = float)
	parser.add_argument("-x", "--xvars", dest = "X", help = "Indices of independent (X) variables", required = True,
						action = "store", type = str)
	parser.add_argument("-y", "--yvar", dest = "Y", help = "Indices of dependent (Y) variable", required = True,
						action = "store", type = int)
	parser.add_argument("-d","--delimiter", dest = "DELIMITER", help = 'Input file delimiter', required = False,
						action = "store", type = str, default = "|")

	# parse arguments
	args = parser.parse_args()
	A = args.A
	B = args.B
	X = parseFields(args.X)
	Y = args.Y - 1
	DELIMITER = args.DELIMITER

	# create risk table
	risk_table = make_risk(fname = sys.stdin, X = X, Y = Y, sep = DELIMITER)

	# dump output to stdout
	print json.dumps(risk_table)

if __name__ == '__main__':
	main()
