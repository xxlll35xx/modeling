from modeling.bayesrisk_create_cli import make_risk, bayes_proportion
from modeling.load_data import data_dir
from modeling.ks import kspy
import json
from os.path import join
import unittest

# ks test data
# no weights
x = [1.26, 0.34, 0.70, 1.75, 50.57, 1.55, 0.08, 0.42, 0.50, 3.20, 0.15, 0.49, 0.95, 0.24, 1.37, 0.17, 6.98, 0.10, 0.94, 0.38]
y = [2.37, 2.16, 14.82, 1.73, 41.04, 0.23, 1.32, 2.91, 39.41, 0.11, 27.44, 4.51, 0.51, 4.50, 0.18, 14.68, 4.66, 1.30, 2.06, 1.19]
t0 = [0 for i in x] + [1 for i in y]
t1 = [1 for i in x] + [0 for i in y]
s = x + y
# with weights
xw = [0, 0, 0, 0, 0, 0, 2, 0, 0, 0]
yw0 = [1, 1, 0, 0, 0, 0, 1, 0, 0, 0]
yw1 = [1 - y for y in yw0]
weights = [1, 2, 4.01725, 1, 3.53498, 8.42887498998,1, 1.47510424332, 3.53498, 1.55276220333]

def test_bayesrisk_create():
	fname = data_dir()
	out1 = make_risk(fname = join(fname, 'data.psv'), X = [0,1], Y = 3, a = 25, b = 75, sep = "|")
	with open(join(fname, 'bayestest.result.json'),'r') as f:
		out2 = json.loads(f.read())
	assert(out1 == out2)

class TestBayesProportion(unittest.TestCase):
	def test_bayes_proportion(self):
		self.assertAlmostEqual(bayes_proportion(8, 20, 4, 3), 0.4444, places = 4)
		self.assertAlmostEqual(bayes_proportion(2, 5, 4, 3), 0.50, places = 2)
		self.assertAlmostEqual(bayes_proportion(400, 1000, 4, 3), 0.4011916583912612, places = 5)

class TestKSPY(unittest.TestCase):
	def test_kspy_noweights(self):
		self.assertAlmostEqual(kspy(s,t0), 0.45, places = 2)
		self.assertAlmostEqual(kspy(s,t1), -0.45, places = 2)
		
	def test_kspy_weights(self):
		self.assertAlmostEqual(kspy(xw, yw0, weights = weights), 0.25, places = 2)
		self.assertAlmostEqual(kspy(xw, yw1, weights = weights), -0.25, places = 2)
