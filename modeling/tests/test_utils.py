from modeling.util import parseFields

def test_parseFields_simple():
	assert(parseFields("1-3") == [0,1,2])

def test_parseFields_complicated():
		assert(parseFields("1-3,6-10,25-30,31") == [0,1,2,5,6,7,8,9,24,25,26,27,28,29,30])

def test_parseFields_singletons():
	assert(parseFields("1,2,3,4,5,6,11,12,13") == [0,1,2,3,4,5,10,11,12])
