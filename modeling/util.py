def parseFields(fieldStr):
	"""
	Parse fields given as linux style indices.

	Params:
		fieldStr (string): linux style indices
	Returns:
		fields (list): list of individual indices
	"""
	fields = []
	parsed = fieldStr.split(",")
	for part in parsed:
		subParsed = part.split("-")
		if len(subParsed) == 1:
			fields.append(int(part) - 1)
		elif len(subParsed) == 2:
			for i in range(int(subParsed[0]) - 1, int(subParsed[1])):
				fields.append(i)
	return fields
