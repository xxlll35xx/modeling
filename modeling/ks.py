import sys
import csv
import argparse
import numpy as np
from util import parseFields
from collections import defaultdict


def kspy(score, tag, weights = None, totalZero = None, totalOne = None):
    """
    Calculate KS
    
    Params:
        score (list or array): independent variable
        tag (list or array): dependent variable
        weights (list or array; optional): weights to use in calculation
    Returns:
        maxKS (float): max separation between CDFs
    """
    # initialize variables
    index = np.argsort(score)[::-1]
    zeroSoFar = 0.0
    oneSoFar = 0.0
    maxKS = 0.0
    lastScore = 0.0
    
    # weights are one if not provided
    if weights is None:
        weights = np.repeat(1., len(score))
    
    # initialize counters
    if totalZero is None:
        totalZero = np.array([w for w,t in zip(weights, tag) if t == 0.]).sum()
    if totalOne is None:
        totalOne = np.array([w for w,t in zip(weights, tag) if t == 1.]).sum()
    
    # KS calculation logic
    for i in index:
        thisScore = score[i]
        thisWeight = weights[i]
        if thisScore != lastScore:
            lastScore = thisScore
            ks = (oneSoFar/totalOne) - (zeroSoFar/totalZero)
            if np.abs(ks) > np.abs(maxKS):
                maxKS = ks
        if tag[i] == 0.0:
            zeroSoFar += thisWeight
        elif tag[i] == 1.0:
            oneSoFar += thisWeight
            
    return maxKS
    
def get_data(filename, ivs, dv, weights = None, sep = ","):
    """
    Read file and store data for ks processing.
    
    Params:
        filename: file to open (file or sys.stdin)
        ivs (string): Linux style indices of ivs
        dv (int): Linux style index of dv
        weights (int): Linux style index of weights column (optional)
        sep (string): column delimiter
    Returns:
        dictionary
        {'ivs':{'var1':[val1, val2, ... ], ...
            },
        'dvs': {'dv_var':[val1, val2, ...]
                }
        'weights': {'weight_var': [val1, val2,...]
                }
        }
    """
        
    
    # determine if reading file or stdin
    if type(filename) == str:
        inFh = open(filename,'rb')
    else:
        inFh = sys.stdin
        
    # parse ivs and dv
    iv_idx = parseFields(str(ivs))
    dv_idx = int(dv) - 1
    # parse weights column
    if weights:
        weights_idx = int(weights) - 1
    
    # instantiate reader
    reader = csv.reader(inFh, delimiter = sep)
    
    # output container
    data = defaultdict(lambda: defaultdict(list))
    # iterate over file
    for i,line in enumerate(reader):
        # gather header information
        if i == 0:
            iv_header = [var for i,var in enumerate(line) if i in iv_idx]
            dv_header = line[dv_idx]
            if weights:
                weights_header = line[weights_idx]
        # append data
        else:
            # append ivs
            for idx,var in zip(iv_idx, iv_header):
                data['ivs'][var].append(float(line[idx]))
            # append dvs
            data['dv'][dv_header].append(float(line[dv_idx]))
            # append weights
            if weights:
                data['weights'][weights_header].append(float(line[weights_idx]))
    
    # close file if not stdin
    if type(filename) == str:
        inFh.close()
    
    return data
            
def printKS(data_dictionary,sep = ","):
    """ Calculates KS from a get_data object; prints to screen."""
    data = data_dictionary
    y = data['dv'][data['dv'].keys()[0]]
    if 'weights' in data:
        weights = data['weights'][data['weights'].keys()[0]]
    else:
        weights = None
    print sep.join(["variable","maxKS"])
    for var in data['ivs']:
        x = data['ivs'][var]
        varKS = kspy(x, y, weights = weights)
        print sep.join([var, str(varKS)])

def main():
    USAGE = "cat FILE | python ks.py [options] > OUTPUT"
    DESCRIPTION = """ Command line KS utility. """
    parser = argparse.ArgumentParser(description = DESCRIPTION, usage = USAGE, formatter_class = argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-x","--x", dest = "X", help = "Independent variables (Linux style indices)", required = True, default = 1, action = "store", type = str)
    parser.add_argument("-y","--y", dest = "Y", help = "Dependent variable (Linux style index)", required = True, default = 2, action = "store", type = int)
    parser.add_argument("-w","--weights", dest = "W", help = "Weights variable (linux style index)", required = False, default = None, action = "store", type = int)
    parser.add_argument("-d","--delimiter", dest = "DELIMITER", help = "File delimiter", required = False, default = ",", action = "store", type = str)
    args = parser.parse_args()
    
    # read data
    data = get_data(filename = sys.stdin, ivs = args.X, dv = args.Y, weights = args.W, sep = args.DELIMITER)
    # print ks to screen
    printKS(data)
    
# main method
if __name__ == "__main__":
    main()
    
    
    
    
    
    
    
    