from os.path import join,dirname

def data_dir():
	filename = join(dirname(__file__), 'data')
	return filename
