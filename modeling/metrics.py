#!/usr/bin/env python

from sklearn.metrics import roc_auc_score, log_loss
from sklearn.preprocessing import MinMaxScaler
from sklearn.linear_model import LogisticRegression
from scipy.stats import ks_2samp
from modeling.util import parseFields
import pandas as pd
import argparse
import sys

def ks(y_true, y_pred):
	return ks_2samp(y_pred[y_true == 0], y_pred[y_true == 1]).statistic

def main():
	USAGE = 'cat FILE | metrics [arguments] > OUTPUT'
	DESCRIPTION = 'Basic metrics for binary classification.'
	parser = argparse.ArgumentParser(description = DESCRIPTION, usage = USAGE, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("-x", "--x", dest = "X", help = "independent variables", required = True, \
						action = "store", type = str)
	parser.add_argument("-y", "--y", dest = "Y", help = "depdendent variable", required = True, \
						action = "store", type = int)
	parser.add_argument("-d", "--delimiter", dest = "DELIMITER", help = "file delimiter", required = True, \
						action = "store", type = str)

	# parse arguments
	args = parser.parse_args()
	X = parseFields(args.X)
	Y = args.Y - 1
	DELIMITER = args.DELIMITER
	
	data = pd.read_csv(sys.stdin, sep = DELIMITER, index_col = None)
	XVARS = [col for i,col in enumerate(data.columns) if i in X]
	YVAR = data.columns[Y]

	print DELIMITER.join(['var','ks','auc','log_loss'])
	y = data[YVAR].values

	for var in XVARS:
		this_var = data[var].values
		model = LogisticRegression()
		model.fit(this_var.reshape((-1,1)), y)
		preds = model.predict_proba(this_var.reshape((-1,1)))[:,1]
		this_ks = ks(y, this_var)
		this_auc = roc_auc_score(y, preds)
		this_log_loss = log_loss(y, preds)

		print DELIMITER.join([var, str(this_ks), str(this_auc), str(this_log_loss)])




