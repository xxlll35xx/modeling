import csv
import sys
from modeling.util import parseFields
import argparse

class Lookup(object):
	"""
	Class to create lookup table in memory.
	"""
	def __init__(self, fname, key, keep, sep = ","):
		"""
		Params:
			fname:  string; path to file
			key: int; Linux style index of lookup key
			keep: string; Linux style indices of columns to keep on output
			sep: string; file delimiter/separator
		"""
		self.fname = str(fname)
		self.key = int(key) - 1
		self.keep = parseFields(str(keep))
		self.sep = sep
		# make sure key is not duplicated in keep fields
		if self.key in self.keep:
			self.keep.remove(self.key)

	def load(self):
		"""
		Method to load file.

		Params:
			None
		Returns:
			None

		Loads file specified at init into self.lookup.
		"""
		with open(self.fname, 'rb') as f:
			reader = csv.reader(f, delimiter = self.sep)
			lookup = dict()
			for i,row in enumerate(reader):
				if i == 0:
					self.header = [col for i,col in enumerate(row) if i in self.keep]
				else:
					key = row[self.key]
					lookup[key] = [col for i,col in enumerate(row) if i in self.keep]
			self.lookup = lookup

class Stream(object):
	"""
	Class to create streaming joining table.
	"""
	def __init__(self, key, keep, sep = ","):
		"""Initialization function:
		
		Params:
			key: int; Linux style index of key field
			keep: str; Linux style indices of fields to keep
			sep: str; field separator
		"""
		self.key = int(key) - 1
		self.keep = parseFields(str(keep))
		self.sep = sep
		# make sure key is not duplicated in keep fields
		if self.key in self.keep:
			self.keep.remove(self.key)
	def stream(self):
		"""
		Stream method

		Returns:
			generator of file key, sorted key + value (i.e., columns to keep) tuple
		"""
		reader = csv.reader(sys.stdin, delimiter = self.sep)
		for i,row in enumerate(reader):
			if i == 0:
				# keep header in order columns occur in file
				self.header_idx = sorted([self.key] + self.keep)
				self.header = [col for i,col in enumerate(row) if i in self.header_idx]
			else:
				output = dict()
				key = row[self.key]
				# keep header + values in same order they occur in file
				values = [col for i,col in enumerate(row) if i in self.header_idx]
				yield key,values

def main():
	"""
	Creates stream and lookup objects; uses loop over stream object to perform join.
	"""
	USAGE = 'cat STREAM | python hashjoin.py [options] LOOKUP > OUTPUT'
	DESCRIPTION = """Command line hashjoin utility in python. 
					Key column names do not need to match across files: joining file key will always be output.
					Output order of columns in both files remains the same.
				"""
	parser = argparse.ArgumentParser(description = DESCRIPTION, usage = USAGE, formatter_class = argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("-j", "--j", dest = "J", help = "Joining (streaming) file key", required = True, default = 1,
						action = "store", type = int)
	parser.add_argument("-k","--k", dest = "K", help = "Lookup file key", required = True, default = 1,
						action = "store", type = int)
	parser.add_argument("-s", "--s", dest = "S", help = "Joining (streaming) columns to keep", required = False, default = "",
						action = "store", type = str)
	parser.add_argument("-t", "--t", dest = "T", help = "Lookup columns to keep", required = False, default = "",
						action = "store", type = str)
	parser.add_argument("-d", "--delimiter", dest = "DELIMITER", help = "File delimiter", required = False, default = ",",
						action = "store", type = str)
	parser.add_argument("-x", "--x", dest = "X", help = "Value to output if value missing in lookup file (only if value is provided)", default = "",
						action = "store", type = str)
	parser.add_argument("LOOKUP", help = "Lookup file", action = "store", type = str)
	# parse arguments
	args = parser.parse_args()
	# instantiate lookup file
	lookup = Lookup(fname = args.LOOKUP, key = args.K, keep = args.T, sep = args.DELIMITER)
	lookup.load()
	# instantiate streaming file
	stream = Stream(key = args.J, keep = args.S, sep = args.DELIMITER)
	# iterate through stream
	# header toggle
	NEED_TO_PRINT_HEADER = True
	# filler variables if needed
	FILLER = [args.X for i in range(len(lookup.keep))]
	for key,values in stream.stream():
		# print header
		if NEED_TO_PRINT_HEADER:
			print args.DELIMITER.join(stream.header + lookup.header)
			NEED_TO_PRINT_HEADER = False
		# check that stream key exists in lookup
		to_join = lookup.lookup.get(key, [])
		# if stream key exists, print to stdout
		if to_join:
			print args.DELIMITER.join(values + lookup.lookup[key])
		elif args.X:
			print args.DELIMITER.join(values + FILLER)


if __name__ == "__main__":
	main()
