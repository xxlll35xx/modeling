#!/usr/bin/env python

import csv
import sys
from modeling.util import parseFields
import argparse
from collections import defaultdict
import json

def apply_risktable(risktable, input_file, keep = None, sep = "|"):
	# open input file
	if isinstance(input_file,str):
		need_to_close_input = True
		inFh = open(input_file,'r')
		reader = csv.DictReader(inFh, delimiter = sep)
	else:
		need_to_close_input = False
		reader = csv.DictReader(sys.stdin, delimiter = "|")
	
	# load risk table
	with open(risktable, 'r') as f:
		risktable = json.loads(f.read())

	# make sure all risktable fields are in input file
	unique_variables = set(reader.fieldnames)
	unique_risk_fields = set([var for var in risktable.keys() if var != 'hyperparameters'])
	assert((unique_variables & unique_risk_fields) == unique_risk_fields)

	# initialize output fields and risk keys
	fields = []
	risk_fields = [var for var in risktable.keys() if var != 'hyperparameters']
	input_fields = reader.fieldnames

	# append keep fields
	if keep:
		keep_fields = [var for i,var in enumerate(input_fields) if i in keep]
		fields = fields + keep_fields
	fields = fields + [var + '_proportion' for var in risk_fields] + [var + '_bayes_proportion' for var in risk_fields]

	# instantiate output dictionary
	output = {}
	output['data'] = []

	# add header to output dictionary
	output['header'] = fields

	# process reader line by line
	for line in reader:
		# instantiate dictionary for data to write, data list for this record
		this_record = {}
		data = []
		# include keep fields if provided
		if keep:
			for var in keep_fields:
				this_record[var] = line[var]
		# incorporate proportion and bayes proportion
		for var in risk_fields:
			current_value = line[var]
			this_record[var + '_proportion'] = risktable[var][current_value]['proportion']
			this_record[var + '_bayes_proportion'] = risktable[var][current_value]['bayes_proportion']
		# append this record to data
		output['data'].append(this_record)
	
	# close files
	if need_to_close_input:
		inFh.close()
	
	return output

def write_output(applied_risk_table, output_file, sep = "|"):
	# create writer
	header = applied_risk_table['header']
	if isinstance(output_file, str):
		need_to_close = True
		oFh = open(output_file,'w')
		writer = csv.DictWriter(oFh, delimiter = sep, fieldnames = header)
	else:
		need_to_close = False
		writer = csv.DictWriter(sys.stdout, delimiter = sep, fieldnames = header)
	writer.writeheader()

	# write one row at a time
	for row in applied_risk_table['data']:
		writer.writerow(row)

	# close file
	if need_to_close:
		oFh.close()

def main():
	# set argparse options
	USAGE = 'cat FILE | bayesrisk_apply_cli [arguments] > OUTPUT'
	DESCRIPTION = 'Create Bayesian risk tables given a categorical variable and a dichomotous outcome.'
	parser = argparse.ArgumentParser(description = DESCRIPTION, usage = USAGE, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("-k","--keep", dest = "KEEP", help = "Additional fields to keep", required = False,
						action = "store", type = str, default = None)
	parser.add_argument("-d","--delimiter", dest = "DELIMITER", help = 'Input file delimiter', required = False,
						action = "store", type = str, default = "|")
	parser.add_argument("RISKTABLE", help = 'Risk table to apply', action = "store", type = str)

	# parse arguments
	args = parser.parse_args()
	if args.KEEP:
		KEEP = parseFields(args.KEEP)
	else:
		KEEP = args.KEEP
	DELIMITER = args.DELIMITER
	RISKTABLE = args.RISKTABLE

	# apply risk table
	applied = apply_risktable(risktable = RISKTABLE, input_file = sys.stdin, keep = KEEP, sep = DELIMITER)

	# format output
	write_output(applied, sys.stdout, sep = DELIMITER)

if __name__ == '__main__':
	main()
