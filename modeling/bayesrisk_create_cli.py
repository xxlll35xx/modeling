#!/usr/bin/env python

import csv
import sys
from modeling.util import parseFields
import argparse
from collections import defaultdict
import json

def bayes_proportion(y,N,a,b):
	"""
	Function to calculate beta-binomial proportion.
	y = number of 'successes'
	N = total number of trials
	a = prior number of 'successes'
	b = prior number of 'failures'
	"""
	y = float(y)
	N = float(N)
	a = float(a)
	b = float(b)

	empirical_mean = y/N
	empirical_weight = N / (N + a + b)

	if a+b > 0:
		prior_mean = a/(a + b)
		prior_weight = (a + b)/(N + a + b)
	else:
		prior_mean = 0
		prior_weight = 0

	return (empirical_mean * empirical_weight) + (prior_mean * prior_weight)


def make_risk(fname, X, Y, a = 0, b = 0, sep = "|"):
	# instantiate file reader
	if isinstance(fname,str):
		inFh = open(fname,'r')
		need_to_close = True
		reader = csv.DictReader(inFh, delimiter = sep)
	else:
		need_to_close = False
		reader = csv.DictReader(fname, delimiter = sep)

	# select independent variables by index
	# check that X indices are valid
	assert(len(X) <= len(reader.fieldnames))
	# check that Y is not included in X
	assert(Y not in X)
	variables = [var for i,var in zip(X, reader.fieldnames) if i in X]
	target = reader.fieldnames[Y]

	# initialize output dictionary
	# structure
	"""
	{
		'variable1': {
			'value1': {
				'1': count of 1's,
				'total': number of records,
				'proportion': '1'/'total
			},
			...,
		},
		'variable2': {
			'value1': {
				...,
			}
		}
	}
	"""
	output = defaultdict(lambda: defaultdict(lambda: defaultdict(float)))

	# process file line by line
	for line in reader:
		# process each variable
		for current_var in variables:
			# set current value
			current_value = line[current_var]
			# set current target
			current_target = float(line[target])
			# add target value to 1 key
			output[current_var][current_value]['1'] += current_target
			# add one to the total
			output[current_var][current_value]['total'] += 1
			# calculate running proportion
			output[current_var][current_value]['proportion'] = output[current_var][current_value]['1']/output[current_var][current_value]['total']
			# calculate running bayes proportion
			output[current_var][current_value]['bayes_proportion'] = bayes_proportion(y = output[current_var][current_value]['1'], N = output[current_var][current_value]['total'], a = a, b = b) 
	
	# close file if open
	if need_to_close:
		inFh.close()

	# keep A and B parameters in risk table
	output['hyperparameters'] = {'A':a, 'B':b}
	
	return dict(output)

def main():
	# set argparse options
	USAGE = 'cat FILE | bayesrisk_create.py [arguments] > OUTPUT'
	DESCRIPTION = 'Create Bayesian risk tables given a categorical variable and a dichomotous outcome.'
	parser = argparse.ArgumentParser(description = DESCRIPTION, usage = USAGE, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("-a", "--a", dest = "A", help = "a parameter for Bayesian smoothing", required = True,
						action = "store", type = float)
	parser.add_argument("-b", "--b", dest = "B", help = "b parameter for Bayesian smoothing", required = True,
						action = "store", type = float)
	parser.add_argument("-x", "--xvars", dest = "X", help = "Indices of independent (X) variables", required = True,
						action = "store", type = str)
	parser.add_argument("-y", "--yvar", dest = "Y", help = "Indices of dependent (Y) variable", required = True,
						action = "store", type = int)
	parser.add_argument("-d","--delimiter", dest = "DELIMITER", help = 'Input file delimiter', required = False,
						action = "store", type = str, default = "|")

	# parse arguments
	args = parser.parse_args()
	A = args.A
	B = args.B
	X = parseFields(args.X)
	Y = args.Y - 1
	DELIMITER = args.DELIMITER

	# create risk table
	risk_table = make_risk(fname = sys.stdin, X = X, Y = Y, a = A, b = B, sep = DELIMITER)

	# dump output to stdout
	print json.dumps(risk_table)

if __name__ == '__main__':
	main()
